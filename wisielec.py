#!/usr/bin/env python3

from time import sleep
from losowe_haslo import losuj_hasło
from ascii_art import rysunek
from komunikaty import Komunikaty

PODKREŚLNIK = '_'
ODSTĘP = '\n\n'
MAKS_PRÓB = 10

def koduj_hasło(hasło, wykorzystane_litery, litery):
    '''Zwraca sformatowane hasło z podkreślnikami
    w miejscu niewykorzystanych liter.'''
    zakodowane_hasło = []

    for znak in hasło:
        if (znak not in litery) or (znak in wykorzystane_litery):
            kod = znak
        else:
            kod = PODKREŚLNIK

        zakodowane_hasło.append(kod)

    return ' '.join(zakodowane_hasło)


def pokaż_dostępne_litery(wykorzystane_litery, litery):
    '''Łańcuch niewykorzystanych liter.'''
    return ' '.join(lit for lit in litery if lit not in wykorzystane_litery)


def szubienica(pozostało_liter,
               pozostało_prób,
               dostępne_litery,
               zakodowane_hasło,
               komunikaty):
    '''Wyświetla stan gry.'''
    print(ODSTĘP)
    print(f'{komunikaty.pozostało_liter()}: {pozostało_liter}.', 
          f'{komunikaty.pozostało_prób()}: {pozostało_prób}')
    print(komunikaty.dostępne_litery())
    print(dostępne_litery)
    print()
    print(zakodowane_hasło)
    print()
    print(rysunek[pozostało_prób])
    print(komunikaty.twoja_litera(), end='')


def ostrzeżenie(tekst, znaczek='#', sec=2):
    d = min(80, len(tekst))
    print()
    print(d * znaczek)
    print(tekst)
    print(d * znaczek)
    print()
    sleep(sec)


def koniec_gry(zakodowane_hasło,
               adres_url,
               status,
               komunikaty):
    '''Komunikaty na koniec gry.'''
    długość = max(len(zakodowane_hasło), len(adres_url))
    print(ODSTĘP)
    print(f' {(długość+2) * "-"} ')
    print(f'| {zakodowane_hasło:<{długość}} |')
    print(f'| {adres_url:<{długość}} |')
    print(f' {(długość+2) * "-"} ')
    if status == 'wygrana':
        print(komunikaty.wygrana())
    else:
        pozostało_prób = 0
        print(rysunek[pozostało_prób])
        print(komunikaty.przegrana())
    print(ODSTĘP)
    
    
def wisielec():
    '''Gra w Wisielca na podstawie tytułu
    losowego hasła z Wikipedii.'''
    komunikaty = Komunikaty()
    print(komunikaty.oczekiwanie_na_hasło())
    hasło, adres_url = losuj_hasło(komunikaty.zwróć_język())
    hasło = hasło.upper()
    liczba_błędów = 0
    pozostało_prób = MAKS_PRÓB
    wykorzystane_litery = ''
    dostępne_litery = pokaż_dostępne_litery(wykorzystane_litery, komunikaty.zwróć_litery())
    zakodowane_hasło = koduj_hasło(hasło, wykorzystane_litery, komunikaty.zwróć_litery())
    pozostało_liter = zakodowane_hasło.count(PODKREŚLNIK)

    while True:
        # Pokaż stan i pobierz literę.
        while True:
            szubienica(pozostało_liter,
                       pozostało_prób,
                       dostępne_litery,
                       zakodowane_hasło,
                       komunikaty)
            litera = input().upper()

            if len(litera) != 1:
                ostrzeżenie(komunikaty.tylko_jedna_litera())
                continue

            if litera not in komunikaty.zwróć_litery():
                ostrzeżenie(komunikaty.wymaganie_litery())
                continue

            if litera in wykorzystane_litery:
                ostrzeżenie(komunikaty.powtarzalność_liter())
                continue

            break

        wykorzystane_litery += litera
        dostępne_litery = pokaż_dostępne_litery(wykorzystane_litery, komunikaty.zwróć_litery())

        if litera in hasło:
            zakodowane_hasło = koduj_hasło(hasło, wykorzystane_litery, komunikaty.zwróć_litery())
            pozostało_liter = zakodowane_hasło.count(PODKREŚLNIK)

            if pozostało_liter == 0:  # Wygrana.
                koniec_gry(zakodowane_hasło,
                           adres_url,
                           'wygrana',
                           komunikaty)
                return

        else:
            liczba_błędów += 1
            pozostało_prób -= 1

            if pozostało_prób == 0:  # Przegrana.
                koniec_gry(koduj_hasło(hasło, komunikaty.zwróć_litery(), komunikaty.zwróć_litery()),
                           adres_url,
                           'przegrana',
                           komunikaty)
                return


if __name__ == '__main__':
    wisielec()
