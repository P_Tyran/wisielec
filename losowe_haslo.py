import requests
from stale import API_URL_PL, WIKI_URL_PL, API_URL_EN, WIKI_URL_EN

SŁOWNIK = {'polski': {'API_URL': API_URL_PL, 'WIKI_URL': WIKI_URL_PL},
           'angielski': {'API_URL': API_URL_EN, 'WIKI_URL': WIKI_URL_EN}}
PARAMS = {
    "action": "query",
    "format": "json",
    "list": "random",
    "rnlimit": "1",
    "rnnamespace": 0  # Strony artykułów.
    }


def losuj_hasło(język):
    '''Zwraca tytuł losowego artykułu
    z Wikipedii dla danego języka i jego adres URL.'''
    r = requests.get(url=SŁOWNIK[język]['API_URL'], params=PARAMS)
    strona = r.json()
    losowa = strona['query']['random'][0]
    id_strony, tytuł = losowa['id'], losowa['title']
    adres_url = '{}?curid={}'.format(SŁOWNIK[język]['WIKI_URL'], id_strony)
    return tytuł, adres_url
